<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget_Testimonials extends Widgets {
    public $title = "Testimonials";
    
    public $description = "A widget that shows testimonials";
    
    public $author = "Igor Olejar";
    
    public $version = "1.0";
    
    public $website = "";
    
    
    public function run($options)
    {
        // get the testimonials
        $result = $this->db->select('*')
                            ->from('testimonial')
                            ->order_by('id', 'asc')
                            ->get();
        
        if ($result->num_rows() > 0) {
            return array('items' => $result->result());
        }
        
        return array('items' => false);
    }
    
    public function form(){}
    
}
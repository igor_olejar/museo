<?php if ($items && is_array($items) && !empty($items)): ?>
<div class="row">
    <div class="nine columns testimonials-list">
        <?php foreach ($items as $item): ?>
            <div class="row">
                <div class="four columns testimonial-image">
                    <?php echo $item->image; ?>
                </div>
                <div class="eight columns">
                    <p>"<?php echo $item->caption; ?>"</p>
                    <p><span class="testimonial-name"><?php echo $item->name; ?></span><br />
                        <?php echo $item->department; ?>,<br />
                        <?php echo $item->institution; ?>
                    </p>
                </div>
            </div>
        <?php endforeach; unset($item); ?>
    </div>
</div>
<?php endif; ?>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget_Ranges extends Widgets {
    public $title = "Ranges";
    
    public $description = "A widget that shows Ranges";
    
    public $author = "Igor Olejar";
    
    public $version = "1.0";
    
    public $website = "";
    
    public $fields = array(
        array(
            'field'     =>  'museum',
            'label'     =>  'Museum',
            'rules'     =>  'required'
        ),
    );
    
    
    public function run($options)
    {
        // get the related ranges
        $result = $this->db->select('*')
                            ->from('ranges')
                            ->where('museum', $options['museum'])
                            ->order_by('id', 'asc')
                            ->get();
        
        $out = array('title' => strtoupper(str_replace("-", " ", $options['museum'])));
        
        if ($result->num_rows() > 0) {
            if ($result->num_rows() == 1) {
                redirect('ranges/' . $result->row()->id);
            }
            
            $out['ranges'] = $result->result();
        } else {
            $out['ranges'] = false;
        }
        
        return $out;
    }
    
    public function form(){
        // get the list of museums
        $out = array();
        
        $result = $this->db->select('title, slug')
                            ->from('pages')
                            ->where('parent_id <> 0')
                            ->where('status="live"')
                            ->order_by('title')
                            ->get();
        
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $out[$row->slug] = $row->title;
            } unset($row);
            
            return array('museums' => $out);
        }
        
        return array('museums' => false);
    }
    
}
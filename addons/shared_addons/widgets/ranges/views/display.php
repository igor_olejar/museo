<div class="row news-item">
    <h3>
        <?php echo $title; ?>
    </h3>
</div>

<?php if ($ranges && is_array($ranges) && !empty($ranges)): ?>
<div class="row ranges-holder">
    <ul>
    <?php $i = 0; foreach ($ranges as $range): ?>
        <li class="three columns <?php if ($i%4 == 0): ?>range-first<?php endif; ?>">
            <div class="range-image">
                <a href="ranges/<?php echo $range->id; ?>">
                <?php echo $range->image; ?>
                </a>
            </div>
            <p class="range-title">
                <a href="ranges/<?php echo $range->id; ?>"><?php echo $range->title; ?></a>
            </p>
        </li>
        <?php $i++; ?>
    <?php endforeach; unset($range); ?>
    </ul>
</div>
<?php else: ?>
    No ranges available to view.
<?php endif; ?>

<?php if ($slides && is_array($slides) && !empty($slides)): ?>
<div class="home-carousel">
    <?php foreach ($slides as $slide): ?>
        <div>
            <?php echo $slide->photo; ?><br />
            <?php echo $slide->caption; ?>
        </div>
    <?php endforeach; unset($slide); ?>
</div>
<?php endif; ?>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget_Slides extends Widgets {
    public $title = "Slides";
    
    public $description = "A widget that creates slides";
    
    public $author = "Igor Olejar";
    
    public $version = "1.0";
    
    public $website = "";
    
    public $fields = array(
        array(
            'field'     =>  'section',
            'label'     =>  'Section',
            'rules'     =>  'required'
        ),
    );
    
    public function run($options)
    {
        // get the related slides
        $result = $this->db->select('*')
                            ->from('slides')
                            ->where('section', $options['section'])
                            ->order_by('order', 'asc')
                            ->get();
        
        if ($result->num_rows() > 0) {
            return array('slides' => $result->result());
        }
        
        return array('slides' => false);
    }
    
    public function form(){}
    
}
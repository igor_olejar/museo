{{ if posts }}
    <?php foreach($posts as $post): ?>
        <div class="row post news-item">
            <div class="five columns news-image">
                <a href="<?php echo $post['news_image']['image']; ?>" class="fancybox" title="<?php echo $post['title']; ?>">
                <?php echo $post['news_image']['img']; ?>
                </a>
            </div>
            <div class="four columns">
                <h3><?php echo $post['title']; ?></h3>
                <p><?php echo $post['intro']; ?></p>
            </div>
        </div>
    <?php endforeach; unset($post); ?>
{{ else }}
	{{ helper:lang line="news:currently_no_posts" }}
{{ endif }}
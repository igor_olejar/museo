

{{ post }}

<div class="row post news-item">

	<h3>{{ title }}</h3>

	<div class="body">
		{{ body }}
	</div>

</div>

{{ /post }}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ranges_m extends MY_Model {
    
    public $ranges_validation_rules = array(
        array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|max_length[200]|required'
        ),
        array(
                'field' => 'museum',
                'label' => 'Museum',
                'rules' => 'trim|max_length[255]|required'
        ),
    );
    
    public function __construct() {
        parent::__construct();
        $this->_table = "ranges";
    }
    
    /**
     * List all items
     * @return array Array of items
     */
    public function getAllItemsAdmin()
    {
        $results = $this->db->get($this->_table)->result();
        
        return $results;
    }
    
    public function createItem() 
    {
        $data = array(
            'title'         =>  $this->input->post('title'),
            'description'   =>  $this->input->post('description'),
            'image'         =>  $this->input->post('image'),
            'museum'        =>  $this->input->post('museum'),
        );
        
        return $this->db->insert($this->_table, $data);
    }
    
    public function getItemById($id)
    {
        $result = $this->db->select('*')
                            ->from($this->_table)
                            ->where('id', $id)
                            ->limit(1)
                            ->get();
        
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        
        return false;
    }
    
    public function editItemById()
    {
        $data = array(
            'title'         =>  $this->input->post('title'),
            'description'   =>  $this->input->post('description'),
            'image'         =>  $this->input->post('image'),
            'museum'        =>  $this->input->post('museum'),
        );
        
        $this->db->where('id', $this->input->post('id'));
        return $this->db->update($this->_table, $data);
    }
    
    public function deleteItemById($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($this->_table);
    }
    
    public function getMuseums()
    {
        $out = array();
        
        $result = $this->db->select('title, slug')
                            ->from('pages')
                            ->where('parent_id <> 0')
                            ->where('status="live"')
                            ->order_by('title')
                            ->get();
        
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $out[$row->slug] = $row->title;
            } unset($row);
            
            return $out;
        }
        
        return false;
    }
    
    public function getRangeById($id)
    {
        $r = $this->db->select('*')
                        ->from('ranges')
                        ->where('id', $id)
                        ->limit(1)
                        ->get();
        
        if ($r->num_rows() > 0) {
            return $r->row();
        }
        
        return false;
    }
    
    public function getOtherRanges($id, $museum)
    {
        $r = $this->db->select('id,title')
                        ->from('ranges')
                        ->where('museum', $museum)
                        ->where('id !=', $id)
                        ->order_by('id')
                        ->get();
        
        $out = array();
        if ($r->num_rows() > 0) {
            foreach ($r->result() as $row) {
                $out[$row->id] = $row->title;
            } unset($row);
        }
        
        return $out;
    }
    
    public function getSlidesByRangeId($id)
    {
        $r = $this->db->select('title,photo,caption,photo_small')
                        ->from('slides')
                        ->where('section', $id)
                        ->order_by('order')
                        ->get();
        
        if ($r->num_rows() > 0) {
            foreach ($r->result() as $res) {
                $fileArray = explode("/large/",$res->photo);
                $fileId = str_replace('" />',"",$fileArray[1]);
                $res->filePath = $this->_findFilePath($fileId);
            } unset($res);
            
            return $r->result();
        }
        
        return false;
    }
    
    private function _findFilePath($fileId)
    {
        $r = $this->db->select('path')
                    ->from('files')
                    ->where('id', $fileId)
                    ->get();
        
        return $r->row()->path;
    }
}
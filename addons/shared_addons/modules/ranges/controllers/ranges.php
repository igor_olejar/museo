<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ranges extends Public_Controller
{
	public $stream;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ranges_m', 'rm');
	}

	/**
	 * Index
	 *
	 * List out the range.
	 */
	public function index()
	{
            redirect("/", true);
	}
        
        public function show($id = 0)
        {
            $next_range = $prev_range = $slides = false;
            
            if (!isset($id) || $id == 0) {
                redirect("/", true);
            }
            
            $range_data = $this->rm->getRangeById($id);
            
            if ($range_data) {
                $other_ranges = $this->rm->getOtherRanges($id, $range_data->museum);
                $next_range = $this->_getNextSiblingRange($other_ranges, $id);
                $prev_range = $this->_getPrevSiblingRange($other_ranges, $id);
                $slides = $this->rm->getSlidesByRangeId($id);
            }
            
            $this->template
			->title($this->module_details['name'])
                        ->set('range', $range_data)
                        ->set('next', $next_range)
                        ->set('prev', $prev_range)
                        ->set('slides', $slides)
                        ->build('range');
        }
        
        private function _getNextSiblingRange($other, $id)
        {
            $range = false;
            $range_ids = array_keys($other);
            
            foreach ($range_ids as $sibling)
            {
                if ($sibling > $id) {
                    $range = $sibling;
                    break;
                }
            } unset($sibling);
            
            if ($range) return $range;
            
            return reset($range_ids);
        }
        
        private function _getPrevSiblingRange($other, $id)
        {
            $range = false;
            $range_ids = array_reverse(array_keys($other));
            
            foreach ($range_ids as $sibling)
            {
                if ($sibling < $id) {
                    $range = $sibling;
                    break;
                }
            } unset($sibling);
            
            if ($range) return $range;
            
            return reset($range_ids);
        }
}

<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends Admin_Controller
{
	protected $section = 'items';
        
        private $_museums = array();

	public function __construct()
	{
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('ranges_m', 'rm');

            $this->form_validation->set_rules($this->rm->ranges_validation_rules);
            
            $this->_museums = $this->rm->getMuseums();
	}

	/**
	 * Show all created testimonials
	 */
	public function index()
	{
            $items = $this->rm->getAllItemsAdmin();          
 
            $this->template
                    ->title($this->module_details['name'])
                    ->set('items' , $items)
                    ->build('admin/index');
	}

	public function create() {
            $data = new stdClass();

            if ($this->input->post() 
                    && $this->form_validation->run() !== FALSE
                    && $this->rm->createItem()
                ) {
                redirect('admin/ranges');
            } else {
                foreach ($this->rm->ranges_validation_rules AS $rule) {
                    $data->{$rule['field']} = $this->input->post($rule['field']);
                }
            }

            $this->pyrocache->delete('rm');
            $this->template
                    ->title($this->module_details['name'])
                    ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
                    ->set('museums', $this->_museums)
                    ->build('admin/add_item', $data);
        }

	/**
	 * Edit news post
	 *
	 * @param int $id The ID of the news post to edit
	 */
	public function edit($id = 0)
	{
            $data = new stdClass();
        
            $item = $this->rm->getItemById($id);

            if ($item === false) {
                redirect('admin/ranges');
            }

            if ($this->input->post() 
                    && $this->form_validation->run() !== FALSE
                    && $this->rm->editItemById()
                ) {
                redirect('admin/ranges');
            } else {
                foreach ($this->rm->ranges_validation_rules AS $rule) {
                    $data->{$rule['field']} = $this->input->post($rule['field']);
                }
            }

            $this->pyrocache->delete('rm');
            $this->template
                    ->title($this->module_details['name'])
                    ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
                    ->set('item' , $item)
                    ->set('museums', $this->_museums)
                    ->build('admin/edit_item');
	}

	

	public function delete($id)
        {
            if ($this->tm->deleteItemById($id)) {
                redirect('admin/ranges', TRUE);
            }

            return false;
        }

}

<section class="title">
    <h4>Add a range</h4>
</section>

<section class="item">
    <?php 
        echo form_open('admin/ranges/create', 'id="ranges" class="crud"');
        include_once 'add_edit_form.php';
        echo form_close(); 
    ?>
</section>

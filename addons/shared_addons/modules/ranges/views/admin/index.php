<section class="title">
	<h4>
            Ranges
            <a href="admin/ranges/create" class="button">Add New Range</a>
        </h4>
</section>

<section class="item">
    <div class="content">
        <?php if (empty($items)): ?>
            <div class="no_data">No items listed. Please add some.</div>
        <?php else: ?>
            <div class="row">
                <table class="striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Museum</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($items as $item): ?>
                        <tr>
                            <td><?php echo $item->title; ?></td>
                            <td><?php echo $item->museum; ?></td>
                            <td>
                                <a class="button blue" href="admin/ranges/edit/<?php echo $item->id; ?>">Edit</a>
                                <a class="button red" href="admin/ranges/delete/<?php echo $item->id; ?>">Delete</a>
                            </td>
                        </tr>
                        <?php endforeach; unset($item); ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</section>
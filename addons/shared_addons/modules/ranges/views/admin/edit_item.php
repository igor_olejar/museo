<section class="title">
    <h4>Edit range</h4>
</section>

<section class="item">
    <?php 
        echo form_open('admin/ranges/edit/'.$item->id, 'id="ranges" class="crud"');
        include_once 'add_edit_form.php';
        echo form_close(); 
    ?>
</section>

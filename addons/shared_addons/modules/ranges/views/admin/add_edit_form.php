<?php if (isset($item->id)): ?>
<input type="hidden" name="id" value="<?php echo $item->id; ?>" />
<?php endif; ?>
<div class="form_inputs">
    <fieldset>
        <ul>
            <li>
                <label for="title">Title</label>
                <div class="input">
                    <?php 
                        if (isset($item->title) && !empty($item->title)) {
                            $value = $item->title;
                        } else {
                            $value = set_value('title');
                        }
                    ?>
                     <input name="title" type="text" value="<?php echo $value; ?>" />
                </div>
            </li>
            <li class="editor">
                <label for="description">Description</label>
                <div class="input">
                     <?php
                        if (isset($item->description) && !empty($item->description)) {
                            $value = $item->description;
                        } else {
                            $value = set_value('description');
                        }
                        
                        $data = array(
                            'name'  =>  'description',
                            'value' =>  $value,
                            'class' =>  'wysiwyg-advanced'
                        );
                        echo form_textarea($data);
                     ?> 
                </div>
            </li>
            <li>
                <label for="image">Image</label>
                <div class="input">
                     <?php

                        if (isset($item->image) && !empty($item->image)) {
                            $value = $item->image;
                        } else {
                            $value = set_value('image');
                        }
                        
                        $data = array(
                            'name'  =>  'image',
                            'value'  => $value,
                            'class' =>  'wysiwyg-advanced'
                        );
                        echo form_textarea($data);
                     ?>
                </div>
            </li>
             <li>
                <label for="name">Museum</label>
                <div class="input">
                    <?php 
                        if (isset($item->museum) && !empty($item->museum)) {
                            $value = $item->museum;
                        } else {
                            $value = set_value('museum');
                        }
                        
                        echo form_dropdown('museum', $museums, $value);
                    ?>
                </div>
            </li>
            <li>
                <?php echo form_submit('submit', 'Save'); ?>
            </li>
        </ul>
    </fieldset>
</div>
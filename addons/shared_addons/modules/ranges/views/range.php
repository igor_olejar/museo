<div class="row news-item">
    <div class="three columns dark-text">
        <h3 class="museum-title range-museum">
            <a href="work/<?php echo $range->museum; ?>"><?php echo strtoupper(str_replace("-", " ", $range->museum)); ?></a>
        </h3>
        <div class="range-description">
            <h4><?php echo strtoupper($range->title); ?></h4>
            <p>
                <?php echo $range->description; ?>
            </p>
        </div>
    </div>
    <div class="eight columns" id="range-slides">
        <?php if ($slides && is_array($slides) && !empty($slides)): ?>
            <div class="range-carousel">
                <?php foreach ($slides as $slide): ?>
                <?php $a = preg_match_all( '|<img.*?src=[\'"](.*?)[\'"].*?>|i', $slide->photo, $matches ); ?>
                    <div>
                        <a href="<?php echo $slide->filePath; ?>" class="fancybox">
                            <?php if ($slide->photo_small != ""): ?>
                                <?php echo $slide->photo_small; ?>
                            <?php else: ?>
                                <img src="<?php echo $slide->filePath; ?>" style="width: 562px;" />
                            <?php endif; ?>
                        </a><br />
                        <?php echo $slide->caption; ?>
                    </div> 
                <?php endforeach; unset($slide); ?>
            </div>
        <?php endif; ?>
        
        <?php if ($next || $prev): ?>
        <div id="range-navigation">
            <a id="range-nav-text" href="ranges/<?php echo $next; ?>">NEXT RANGE</a>
            &nbsp;
            <a id="prev-range-link" href="ranges/<?php echo $prev; ?>"><</a>&nbsp;&nbsp;
            <a id="next-range-link" href="ranges/<?php echo $next; ?>">></a>
        </div>
        <?php endif; ?>
    </div>
</div>
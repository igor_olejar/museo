<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Ranges module
 *
 * @author  Igor Olejar
 */
class Module_Ranges extends Module
{
	public $version = '1.0.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Ranges',
			),
			'description' => array(
				'en' => 'Range entries.',
			),
			'frontend' => true,
			'backend' => true,
			'skip_xss' => true,
			'menu' => 'content',

			'roles' => array(
				'put_live', 'edit_live', 'delete_live'
			),

			'sections' => array(
				'items' => array(
					'name' => 'Ranges',
					'uri' => 'admin/ranges',
					'shortcuts' => array(
						array(
							'name' => 'Add New',
							'uri' => 'admin/ranges/create',
							'class' => 'add',
						),
					),
				),
			),
		);

		return $info;
	}

	public function install()
	{
            $this->dbforge->drop_table('ranges');
            $this->db->delete('settings', array('module' => 'ranges'));

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'auto_increment' => true
                ),
                'title' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '200'
                ),
                'image' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'description' => array(
                    'type' => 'TEXT'
                ),
                'museum' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);

            if ( ! $this->dbforge->create_table('ranges'))
            {
                return false;
            }

            if ( ! is_dir($this->upload_path.'ranges') AND ! @mkdir($this->upload_path.'ranges',0777,true))
            {
                return false;
            }

            return true;
            
	}

	public function uninstall()
	{
            $this->dbforge->drop_table('ranges');
 
            $this->db->delete('settings', array('module' => 'ranges'));

            return true;
	}

	public function upgrade($old_version)
	{
		return true;
	}
}

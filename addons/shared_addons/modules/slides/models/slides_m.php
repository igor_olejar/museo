<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Slides_m extends MY_Model {
    
    public $slides_validation_rules = array(
        array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|max_length[100]'
        ),
        array(
                'field' => 'photo',
                'label' => 'Picture',
                'rules' => 'trim|max_length[255]|required'
        ),
        array(
                'field' => 'caption',
                'label' => 'Caption',
                'rules' => 'trim|max_length[255]'
        ),
    );
    
    public function __construct() {
        parent::__construct();
        $this->_table = "slides";
    }
    
    /**
     * List all slides
     * @return array Array of slides
     */
    public function getAllSlidesAdmin()
    {
        $results = $this->db->get($this->_table)->result();
        
        return $results;
    }
    
    public function createSlide() 
    {
        $data = array(
            'title'         =>  $this->input->post('title'),
            'caption'       =>  $this->input->post('caption'),
            'photo'         =>  $this->input->post('photo'),
            'photo_small'   =>  $this->input->post('photo_small'),
            'order'         =>  $this->input->post('order'),
            'section'       =>  $this->input->post('section')
        );
        
        return $this->db->insert('slides', $data);
    }
    
    public function getSlideById($id)
    {
        $result = $this->db->select('*')
                            ->from('slides')
                            ->where('id', $id)
                            ->limit(1)
                            ->get();
        
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        
        return false;
    }
    
    public function editSlide()
    {
        $data = array(
            'title'         =>  $this->input->post('title'),
            'caption'       =>  $this->input->post('caption'),
            'photo'         =>  $this->input->post('photo'),
            'photo_small'   =>  $this->input->post('photo'),
            'order'         =>  $this->input->post('order'),
            'section'       =>  $this->input->post('section')
        );
        
        $this->db->where('id', $this->input->post('id'));
        return $this->db->update('slides', $data);
    }
    
    public function deleteSlideById($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('slides');
    }
    
    public function getAllRanges()
    {
        $out = array();
        
        $r = $this->db->select('id,title')
                        ->from('ranges')
                        ->order_by('title')
                        ->get();
        
        if ($r->num_rows() > 0) {
            foreach ($r->result() as $row) {
                $out[$row->id] = $row->title;
            }
        }
        
        return $out;
     
    }
}
<section class="title">
    <h4>Edit slide</h4>
</section>

<section class="item">
    <?php 
        echo form_open('admin/slides/edit/'.$slide->id, 'id="slides" class="crud"');
        include_once 'add_edit_form.php';
        echo form_close(); 
    ?>
</section>

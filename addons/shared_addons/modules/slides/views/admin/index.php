<section class="title">
	<h4>
            <?php echo 'Slides'; ?>
            <a href="admin/slides/create" class="button">Add New Slide</a>
        </h4>
</section>

<section class="item">
    <div class="content">
        <?php if (empty($slides)): ?>
            <div class="no_data">No slides listed. Please add some</div>
        <?php else: ?>
            <div class="row">
                <table class="striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($slides as $slide): ?>
                        <tr>
                            <td><?php echo $slide->title; ?></td>
                            <td>
                                <a class="button blue" href="admin/slides/edit/<?php echo $slide->id; ?>">Edit</a>
                                <a class="button red" href="admin/slides/delete/<?php echo $slide->id; ?>">Delete</a>
                            </td>
                        </tr>
                        <?php endforeach; unset($slide); ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</section>
<?php if (isset($slide->id)): ?>
<input type="hidden" name="id" value="<?php echo $slide->id; ?>" />
<?php endif; ?>
<div class="form_inputs">
    <fieldset>
        <ul>
            <li>
                <label for="title">Title</label>
                <div class="input">
                    <?php 
                        if (isset($slide->title) && !empty($slide->title)) {
                            $value = $slide->title;
                        } else {
                            $value = set_value('title');
                        }
                    ?>
                     <input name="title" type="text" value="<?php echo $value; ?>" />
                </div>
            </li>
            <li class="editor">
                <label for="caption">Caption</label>
                <div class="input">
                     <?php
                        if (isset($slide->caption) && !empty($slide->caption)) {
                            $value = $slide->caption;
                        } else {
                            $value = set_value('caption');
                        }
                        
                        $data = array(
                            'name'  =>  'caption',
                            'value' =>  $value,
                            'class' =>  'wysiwyg-advanced'
                        );
                        echo form_textarea($data);
                     ?> 
                </div>
            </li>
            <li>
                <label for="photo">Picture</label>
                <div class="input">
                     <?php

                        if (isset($slide->photo) && !empty($slide->photo)) {
                            $value = $slide->photo;
                        } else {
                            $value = set_value('photo');
                        }
                        
                        $data = array(
                            'name'  =>  'photo',
                            'value'  => $value,
                            'class' =>  'wysiwyg-advanced'
                        );
                        echo form_textarea($data);
                     ?>
                </div>
            </li>
            <li>
                <label for="photo">Small Picture</label>
                <div class="input">
                     <?php

                        if (isset($slide->photo_small) && !empty($slide->photo_small)) {
                            $value = $slide->photo_small;
                        } else {
                            $value = set_value('photo_small');
                        }
                        
                        $data = array(
                            'name'  =>  'photo_small',
                            'value'  => $value,
                            'class' =>  'wysiwyg-advanced'
                        );
                        echo form_textarea($data);
                     ?>
                </div>
            </li>
            <li>
                <label for="order">Order (slides are shown in ascending order. Enter number, e.g 1, 2, or 54)   </label>
                <div class="input">
                    <?php 
                        if (isset($slide->order) && !empty($slide->order)) {
                            $value = $slide->order;
                        } else {
                            $value = set_value('order');
                        }
                    ?>
                     <input name="order" type="text" value="<?php echo $value; ?>" />
                </div>
            </li>
            <li>
                <label for="section">Section</label>
                <div class="input">
                    <?php
                    
                        if (isset($slide->section) && !empty($slide->section)) {
                            $value = $slide->section;
                        } else {
                            $value = set_value('section');
                        }
                        
                        $options = array(
                            'home' => 'Home Page',
                        );
                        
                        foreach ($ranges as $key=>$range) {
                            $options[$key] = $range;
                        } unset($range);
                        
                        echo form_dropdown('section', $options, $value);
                    ?>
                </div>
            </li>
            <li>
                <?php echo form_submit('submit', 'Save'); ?>
            </li>
        </ul>
    </fieldset>
</div>
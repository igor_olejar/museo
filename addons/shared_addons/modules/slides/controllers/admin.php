<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends Admin_Controller {
 
    protected $section = "item";
    private $_ranges = array();
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('slides_m', 'sm');
        
        $this->form_validation->set_rules($this->sm->slides_validation_rules);
        
        $this->_ranges = $this->sm->getAllRanges();
    }
 
    function index()
    {
        $slides = $this->sm->getAllSlidesAdmin();          
 
        $this->template
                ->title($this->module_details['name'])
                ->set('slides' , $slides)
                ->build('admin/index');
     }
     
     public function create() {
        $data = new stdClass();
        
        if ($this->input->post() 
                && $this->form_validation->run() !== FALSE
                && $this->sm->createSlide()
            ) {
            redirect('admin/slides');
        } else {
            foreach ($this->sm->slides_validation_rules AS $rule) {
                $data->{$rule['field']} = $this->input->post($rule['field']);
            }
        }
        
        $this->pyrocache->delete('sm');
        $this->template
                ->title($this->module_details['name'])
                ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
                ->set('ranges', $this->_ranges)
                ->build('admin/add_slide', $data);
    }
    
    public function edit($id) 
    {
        $data = new stdClass();
        
        $slide = $this->sm->getSlideById($id);
        
        if ($slide === false) {
            redirect('admin/slides');
        }
        
        if ($this->input->post() 
                && $this->form_validation->run() !== FALSE
                && $this->sm->editSlide()
            ) {
            redirect('admin/slides');
        } else {
            foreach ($this->sm->slides_validation_rules AS $rule) {
                $data->{$rule['field']} = $this->input->post($rule['field']);
            }
        }
        
        $this->pyrocache->delete('sm');
        $this->template
                ->title($this->module_details['name'])
                ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
                ->set('slide' , $slide)
                ->set('ranges', $this->_ranges)
                ->build('admin/edit_slide');
    }
    
    public function delete($id)
    {
        if ($this->sm->deleteSlideById($id)) {
            redirect('admin/slides', TRUE);
        }
        
        return false;
    }
}
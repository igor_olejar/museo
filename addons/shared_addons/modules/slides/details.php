<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Slides extends Module {
    public $version = "1.0";
    
    public function info()
    {
        return array(
            'name' => array(
                'en' => 'Slides'
            ),
            'description' => array(
                'en' => 'This is a Slides module.'
            ),
            'frontend' => false,
            'backend' => true,
            'menu' => 'content',
            'sections' => array(
                'items' => array( 
                    'name'  => 'Slides', 
                    'uri'   => 'admin/slides',
                        'shortcuts' => array(
                            'create' => array(
                                'name'  => 'Create',
                                'uri'   => 'admin/slides/create',
                                'class' => 'add'
                                )
                            )
                        )
                )
        );
    }
    
    public function install()
    {
        $this->dbforge->drop_table('slides');
        $this->db->delete('settings', array('module' => 'slides'));
 
        $slides = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'photo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'photo_small' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'caption' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'order' => array(
              'type' => 'INT'  
            ),
            'section' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            ),
        );
 
        $slides_setting = array(
            'slug' => 'slides_setting',
            'title' => 'Slides Setting',
            'description' => 'A Yes or No option for the Slides module',
            'default' => '1',
            'value' => '1',
            'type' => 'select',
            'options' => '1=Yes|0=No',
            'is_required' => 1,
            'is_gui' => 1,
            'module' => 'slides'
        );
 
        $this->dbforge->add_field($slides);
        $this->dbforge->add_key('id', true);
 
        if ( ! $this->dbforge->create_table('slides') OR ! $this->db->insert('settings', $slides_setting))
        {
            return false;
        }
 
        if ( ! is_dir($this->upload_path.'slides') AND ! @mkdir($this->upload_path.'slides',0777,true))
        {
            return false;
        }
 
        return true;
    }
 
    public function uninstall()
    {
        $this->dbforge->drop_table('slides');
 
        $this->db->delete('settings', array('module' => 'slides'));

        return true;
    }
 
 
    public function upgrade($old_version)
    {
        return true;
    }
 
    public function help()
    {
        parent::help();
    }
}
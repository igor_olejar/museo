<section class="title">
    <h4>Edit testimonial</h4>
</section>

<section class="item">
    <?php 
        echo form_open('admin/testimonial/edit/'.$item->id, 'id="testimonial" class="crud"');
        include_once 'add_edit_form.php';
        echo form_close(); 
    ?>
</section>

<section class="title">
    <h4>Add a testimonial</h4>
</section>

<section class="item">
    <?php 
        echo form_open('admin/testimonial/create', 'id="testimonial" class="crud"');
        include_once 'add_edit_form.php';
        echo form_close(); 
    ?>
</section>

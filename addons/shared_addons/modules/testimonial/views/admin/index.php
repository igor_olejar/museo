<section class="title">
	<h4>
            Testimonials
            <a href="admin/testimonial/create" class="button">Add New Testimonial</a>
        </h4>
</section>

<section class="item">
    <div class="content">
        <?php if (empty($items)): ?>
            <div class="no_data">No items listed. Please add some.</div>
        <?php else: ?>
            <div class="row">
                <table class="striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Name</th>
                            <th>Department</th>
                            <th>Institution</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($items as $item): ?>
                        <tr>
                            <td><?php echo $item->title; ?></td>
                            <td><?php echo $item->name; ?></td>
                            <td><?php echo $item->department; ?></td>
                            <td><?php echo $item->institution; ?></td>
                            <td>
                                <a class="button blue" href="admin/testimonial/edit/<?php echo $item->id; ?>">Edit</a>
                                <a class="button red" href="admin/testimonial/delete/<?php echo $item->id; ?>">Delete</a>
                            </td>
                        </tr>
                        <?php endforeach; unset($item); ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</section>
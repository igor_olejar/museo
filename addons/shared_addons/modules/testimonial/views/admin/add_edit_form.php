<?php if (isset($item->id)): ?>
<input type="hidden" name="id" value="<?php echo $item->id; ?>" />
<?php endif; ?>
<div class="form_inputs">
    <fieldset>
        <ul>
            <li>
                <label for="title">Title</label>
                <div class="input">
                    <?php 
                        if (isset($item->title) && !empty($item->title)) {
                            $value = $item->title;
                        } else {
                            $value = set_value('title');
                        }
                    ?>
                     <input name="title" type="text" value="<?php echo $value; ?>" />
                </div>
            </li>
            <li class="editor">
                <label for="caption">Caption</label>
                <div class="input">
                     <?php
                        if (isset($item->caption) && !empty($item->caption)) {
                            $value = $item->caption;
                        } else {
                            $value = set_value('caption');
                        }
                        
                        $data = array(
                            'name'  =>  'caption',
                            'value' =>  $value,
                            'class' =>  'wysiwyg-advanced'
                        );
                        echo form_textarea($data);
                     ?> 
                </div>
            </li>
            <li>
                <label for="image">Image</label>
                <div class="input">
                     <?php

                        if (isset($item->image) && !empty($item->image)) {
                            $value = $item->image;
                        } else {
                            $value = set_value('image');
                        }
                        
                        $data = array(
                            'name'  =>  'image',
                            'value'  => $value,
                            'class' =>  'wysiwyg-advanced'
                        );
                        echo form_textarea($data);
                     ?>
                </div>
            </li>
             <li>
                <label for="name">Name</label>
                <div class="input">
                    <?php 
                        if (isset($item->name) && !empty($item->name)) {
                            $value = $item->name;
                        } else {
                            $value = set_value('name');
                        }
                    ?>
                     <input name="name" type="text" value="<?php echo $value; ?>" />
                </div>
            </li>
            <li>
                <label for="department">Department</label>
                <div class="input">
                    <?php 
                        if (isset($item->department) && !empty($item->department)) {
                            $value = $item->department;
                        } else {
                            $value = set_value('department');
                        }
                    ?>
                     <input name="department" type="text" value="<?php echo $value; ?>" />
                </div>
            </li>
             <li>
                <label for="institution">Institution</label>
                <div class="input">
                    <?php 
                        if (isset($item->institution) && !empty($item->institution)) {
                            $value = $item->institution;
                        } else {
                            $value = set_value('institution');
                        }
                    ?>
                     <input name="institution" type="text" value="<?php echo $value; ?>" />
                </div>
            </li>
            <li>
                <?php echo form_submit('submit', 'Save'); ?>
            </li>
        </ul>
    </fieldset>
</div>
<?php defined('BASEPATH') or exit('No direct script access allowed');

class Testimonial extends Public_Controller
{
	public $stream;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('testimonial_m', 'tm');
		$this->load->driver('Streams');

		$this->stream = $this->streams_m->get_stream('testimonial', true, 'testimonial');
	}

	/**
	 * Index
	 *
	 * List out the testimonials.
	 */
	public function index()
	{
	}
}

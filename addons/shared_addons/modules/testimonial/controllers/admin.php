<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends Admin_Controller
{
	protected $section = 'items';

	public function __construct()
	{
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('testimonial_m', 'tm');

            $this->form_validation->set_rules($this->tm->testimonial_validation_rules);
	}

	/**
	 * Show all created testimonials
	 */
	public function index()
	{
            $items = $this->tm->getAllItemsAdmin();          
 
            $this->template
                    ->title($this->module_details['name'])
                    ->set('items' , $items)
                    ->build('admin/index');
	}

	public function create() {
            $data = new stdClass();

            if ($this->input->post() 
                    && $this->form_validation->run() !== FALSE
                    && $this->tm->createItem()
                ) {
                redirect('admin/testimonial');
            } else {
                foreach ($this->tm->testimonial_validation_rules AS $rule) {
                    $data->{$rule['field']} = $this->input->post($rule['field']);
                }
            }

            $this->pyrocache->delete('tm');
            $this->template
                    ->title($this->module_details['name'])
                    ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
                    ->build('admin/add_item', $data);
        }

	/**
	 * Edit news post
	 *
	 * @param int $id The ID of the news post to edit
	 */
	public function edit($id = 0)
	{
            $data = new stdClass();
        
            $item = $this->tm->getItemById($id);

            if ($item === false) {
                redirect('admin/testimonial');
            }

            if ($this->input->post() 
                    && $this->form_validation->run() !== FALSE
                    && $this->tm->editItemById()
                ) {
                redirect('admin/testimonial');
            } else {
                foreach ($this->tm->testimonial_validation_rules AS $rule) {
                    $data->{$rule['field']} = $this->input->post($rule['field']);
                }
            }

            $this->pyrocache->delete('tm');
            $this->template
                    ->title($this->module_details['name'])
                    ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
                    ->set('item' , $item)
                    ->build('admin/edit_item');
	}

	

	public function delete($id)
        {
            if ($this->tm->deleteItemById($id)) {
                redirect('admin/testimonial', TRUE);
            }

            return false;
        }

}

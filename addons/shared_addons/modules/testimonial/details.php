<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Testimonial module
 *
 * @author  Igor Olejar
 */
class Module_Testimonial extends Module
{
	public $version = '1.0.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Testimonials',
			),
			'description' => array(
				'en' => 'Testimonial entries.',
			),
			'frontend' => true,
			'backend' => true,
			'skip_xss' => true,
			'menu' => 'content',

			'roles' => array(
				'put_live', 'edit_live', 'delete_live'
			),

			'sections' => array(
				'posts' => array(
					'name' => 'Testimonials',
					'uri' => 'admin/testimonial',
					'shortcuts' => array(
						array(
							'name' => 'Add New',
							'uri' => 'admin/testimonial/create',
							'class' => 'add',
						),
					),
				),
			),
		);

		return $info;
	}

	public function install()
	{
            $this->dbforge->drop_table('testimonial');
            $this->db->delete('settings', array('module' => 'testimonial'));

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'auto_increment' => true
                ),
                'title' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '200'
                ),
                'image' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'caption' => array(
                    'type' => 'TEXT'
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100'
                ),
                'department' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'institution' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', true);

            if ( ! $this->dbforge->create_table('testimonial'))
            {
                return false;
            }

            if ( ! is_dir($this->upload_path.'testimonial') AND ! @mkdir($this->upload_path.'testimonial',0777,true))
            {
                return false;
            }

            return true;
            
	}

	public function uninstall()
	{
            $this->dbforge->drop_table('testimonial');
 
            $this->db->delete('settings', array('module' => 'testimonial'));

            return true;
	}

	public function upgrade($old_version)
	{
		return true;
	}
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial_m extends MY_Model {
    
    public $testimonial_validation_rules = array(
        array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|max_length[100]'
        ),
    );
    
    public function __construct() {
        parent::__construct();
        $this->_table = "testimonial";
    }
    
    /**
     * List all items
     * @return array Array of items
     */
    public function getAllItemsAdmin()
    {
        $results = $this->db->get($this->_table)->result();
        
        return $results;
    }
    
    public function createItem() 
    {
        $data = array(
            'title'     =>  $this->input->post('title'),
            'caption'   =>  $this->input->post('caption'),
            'image'     =>  $this->input->post('image'),
            'name'      =>  $this->input->post('name'),
            'department'   =>  $this->input->post('department'),
            'institution'  =>  $this->input->post('institution'),
        );
        
        return $this->db->insert('testimonial', $data);
    }
    
    public function getItemById($id)
    {
        $result = $this->db->select('*')
                            ->from('testimonial')
                            ->where('id', $id)
                            ->limit(1)
                            ->get();
        
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        
        return false;
    }
    
    public function editItemById()
    {
        $data = array(
            'title'     =>  $this->input->post('title'),
            'caption'   =>  $this->input->post('caption'),
            'image'     =>  $this->input->post('image'),
            'name'      =>  $this->input->post('name'),
            'department'   =>  $this->input->post('department'),
            'institution'  =>  $this->input->post('institution'),
        );
        
        $this->db->where('id', $this->input->post('id'));
        return $this->db->update('testimonial', $data);
    }
    
    public function deleteItemById($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('testimonial');
    }
}
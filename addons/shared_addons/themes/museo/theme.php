<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Museo extends Theme
{
    public $name			= 'Museo';
    public $author			= 'Craig Yamey';
    public $author_website              = 'http://craigyamey.com/';
    public $website			= 'http://craigyamey.com/';
    public $description                 = '';
    public $version			= '1.0.0';
}

/* End of file theme.php */
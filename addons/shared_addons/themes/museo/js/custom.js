$(document).ready(function(){
    $('.home-carousel').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        dots: false
    });
    
    $('.range-carousel').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        dots: true
    });

    $('li.has_children').hover(
        function(){
            $('ul.dropdown').toggle();
        },
        function(){
            $('ul.dropdown').toggle();
        }
    );
        
    $('#prev-range-link').hover(function(){
        $('#range-nav-text').html("PREVIOUS RANGE").attr('href',$(this).attr('href'));
    });
    
    $('#next-range-link').hover(function(){
        $('#range-nav-text').html("NEXT RANGE").attr('href',$(this).attr('href'));
    });
    
    $('li.has_children > a').click(function(e){
        e.preventDefault();
    });
    
    $('.fancybox').fancybox({
        padding: 2
    });
        
});